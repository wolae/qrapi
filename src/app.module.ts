import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { Qr } from "./qr/entity/qr.entity";
import { QrModule } from "./qr/qr.module";
import { UsersModule } from "./users/users.module";
import { AuthenticationModule } from "./authentication/authentication.module";
import { User } from "./users/entity/user.entity";

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "mysql",
      host: "localhost",
      port: 3306,
      username: "root",
      //username: 'admin_qr',
      //password: '5O~1qi8q',
      password: "password",
      database: "api_qr",
      entities: [Qr, User],
      synchronize: true,
    }),
    QrModule,
    UsersModule,
    AuthenticationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
