import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert } from "typeorm";
import * as crypto from "crypto";

@Entity()
export class User {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  name: string;

  @Column({ default: "" })
  avatar: string;

  @Column({ unique: true })
  email: string;

  @BeforeInsert()
  hashPassword() {
    this.password = crypto.createHmac("sha256", this.password).digest("hex");
  }
  @Column()
  password: string;
}
