import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "src/users/entity/user.entity";
import { UsersService } from "src/users/service/users.service";
import { AuthenticationService } from "./service/authentication.service";
import { AuthenticationController } from "./controller/authentication.controller";

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    JwtModule.register({
      secretOrPrivateKey: "secret12356789",
    }),
  ],
  providers: [UsersService, AuthenticationService],
  controllers: [AuthenticationController, AuthenticationController],
})
export class AuthenticationModule {}
