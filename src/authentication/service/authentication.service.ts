import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import CreateUserDto from "src/users/dto/users.dto";
import { User } from "src/users/entity/user.entity";
import { UsersService } from "src/users/service/users.service";
import LoginDto from "../dto/login.dto";
import * as crypto from "crypto";
import { Payload } from "src/core/models/payload.model";

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService
  ) {}

  private async validate(userData: LoginDto): Promise<User> {
    return await this.userService.findByEmail(userData.email);
  }

  public async login(user: LoginDto): Promise<any | { status: number }> {
    return this.validate(user).then((userData) => {
      if (!userData) {
        return { status: 404 };
      }

      const hashedPassword = crypto
        .createHmac("sha256", user.password)
        .digest("hex");

      if (hashedPassword !== userData.password) {
        return { status: 403 };
      }
      let payload = {
        id: `${userData.id}`,
        exp: this.setExpireDate(),
      };
      const accessToken = this.jwtService.sign(payload);

      return {
        expires_in: 3600,
        access_token: accessToken,
        payload: payload,
        status: 200,
      };
    });
  }

  public async register(user: CreateUserDto): Promise<any> {
    user.password = crypto.createHmac("sha256", user.password).digest("hex");
    return this.userService.create(user);
  }

  public async delete(user: User): Promise<any> {
    return this.userService.delete(user);
  }

  private setExpireDate(): number {
    const creationDate: Date = new Date();
    return creationDate.setHours(creationDate.getHours() + 1);
  }
}
