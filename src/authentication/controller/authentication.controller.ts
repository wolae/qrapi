import { Body, Controller, Delete, Post } from "@nestjs/common";
import CreateUserDto from "src/users/dto/users.dto";
import { User } from "src/users/entity/user.entity";
import LoginDto from "../dto/login.dto";
import { AuthenticationService } from "../service/authentication.service";

@Controller("authentication")
export class AuthenticationController {
  constructor(private readonly authService: AuthenticationService) {}

  @Post("login")
  async login(@Body() user: LoginDto): Promise<any> {
    return this.authService.login(user);
  }

  @Post("register")
  async register(@Body() user: CreateUserDto): Promise<any> {
    return this.authService.register(user);
  }
  @Delete()
  async delete(@Body() user: User): Promise<any> {
    return this.authService.delete(user);
  }
}
