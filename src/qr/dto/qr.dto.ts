export class QrDto {
 
    user: string
  

    type: string;
  
 
    expires: string;

 
    uses: string;
  

    isActive: boolean;
}