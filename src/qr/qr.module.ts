import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QrController } from './controller/qr.controller';
import { Qr } from './entity/qr.entity';
import { QrService } from './service/qr.service';

@Module({
  imports:[TypeOrmModule.forFeature([Qr])],
  controllers: [QrController],
  providers: [QrService]
})
export class QrModule {}
