import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { QrDto } from '../dto/qr.dto';
import { Qr } from '../entity/qr.entity';

@Injectable()
export class QrService {
    constructor(
        @InjectRepository(Qr)
        private qrRepository: Repository<Qr>,
      ) {}
    
      findAll(): Promise<Qr[]> {
        return this.qrRepository.find();
      }
    
      findOne(uuid: string): Promise<Qr> {
        return this.qrRepository.findOne(uuid);
      }

      create(dto: QrDto): Promise<Qr> {
          return this.qrRepository.save(dto)
      }
    
       remove(uuid: string): Promise<DeleteResult> {
         return this.qrRepository.delete(uuid);
      }
}
