import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DeleteResult } from 'typeorm';
import { QrDto } from '../dto/qr.dto';
import { Qr } from '../entity/qr.entity';
import { QrService } from '../service/qr.service';
@ApiTags('QR')
@Controller('qr')
export class QrController {

    constructor(private qrService: QrService){}
    
    @Get()
    findAllQr():Promise<Qr[]>{
        return  this.qrService.findAll()
    }

    @Get(':uuid')
    findByUuid(@Param('uuid') uuid:string):Promise<Qr>{
        return this.qrService.findOne(uuid)
    }

    @Post()
    create(@Body() dto: QrDto){
        return this.qrService.create(dto)
    }

    @Delete(':uuid') 
    delete(@Param('uuid') uuid:string): Promise<DeleteResult>{
        return this.qrService.remove(uuid)
    }
}
