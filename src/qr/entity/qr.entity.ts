
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Qr {
    @PrimaryGeneratedColumn("uuid")
    uuid: string;

    @Column()
    user: string
  
    @Column()
    type: string;
  
    @Column()
    expires: string;

    @Column()
    uses: string;
  
    @Column({ default: true })
    isActive: boolean;
}