import CreateUserDto from "src/users/dto/users.dto";
import { User } from "src/users/entity/user.entity";
import LoginDto from "../dto/login.dto";
import { AuthenticationService } from "../service/authentication.service";
export declare class AuthenticationController {
    private readonly authService;
    constructor(authService: AuthenticationService);
    login(user: LoginDto): Promise<any>;
    register(user: CreateUserDto): Promise<any>;
    delete(user: User): Promise<any>;
}
