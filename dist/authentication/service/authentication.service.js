"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const users_dto_1 = require("../../users/dto/users.dto");
const user_entity_1 = require("../../users/entity/user.entity");
const users_service_1 = require("../../users/service/users.service");
const crypto = require("crypto");
const payload_model_1 = require("../../core/models/payload.model");
let AuthenticationService = class AuthenticationService {
    constructor(userService, jwtService) {
        this.userService = userService;
        this.jwtService = jwtService;
    }
    async validate(userData) {
        return await this.userService.findByEmail(userData.email);
    }
    async login(user) {
        return this.validate(user).then((userData) => {
            if (!userData) {
                return { status: 404 };
            }
            const hashedPassword = crypto
                .createHmac("sha256", user.password)
                .digest("hex");
            if (hashedPassword !== userData.password) {
                return { status: 403 };
            }
            let payload = {
                id: `${userData.id}`,
                exp: this.setExpireDate(),
            };
            const accessToken = this.jwtService.sign(payload);
            return {
                expires_in: 3600,
                access_token: accessToken,
                payload: payload,
                status: 200,
            };
        });
    }
    async register(user) {
        user.password = crypto.createHmac("sha256", user.password).digest("hex");
        return this.userService.create(user);
    }
    async delete(user) {
        return this.userService.delete(user);
    }
    setExpireDate() {
        const creationDate = new Date();
        return creationDate.setHours(creationDate.getHours() + 1);
    }
};
AuthenticationService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        jwt_1.JwtService])
], AuthenticationService);
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map