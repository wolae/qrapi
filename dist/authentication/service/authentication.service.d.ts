import { JwtService } from "@nestjs/jwt";
import CreateUserDto from "src/users/dto/users.dto";
import { User } from "src/users/entity/user.entity";
import { UsersService } from "src/users/service/users.service";
import LoginDto from "../dto/login.dto";
export declare class AuthenticationService {
    private readonly userService;
    private readonly jwtService;
    constructor(userService: UsersService, jwtService: JwtService);
    private validate;
    login(user: LoginDto): Promise<any | {
        status: number;
    }>;
    register(user: CreateUserDto): Promise<any>;
    delete(user: User): Promise<any>;
    private setExpireDate;
}
