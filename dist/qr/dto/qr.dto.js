"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QrDto = void 0;
const openapi = require("@nestjs/swagger");
class QrDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { user: { required: true, type: () => String }, type: { required: true, type: () => String }, expires: { required: true, type: () => String }, uses: { required: true, type: () => String }, isActive: { required: true, type: () => Boolean } };
    }
}
exports.QrDto = QrDto;
//# sourceMappingURL=qr.dto.js.map