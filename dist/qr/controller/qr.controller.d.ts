import { DeleteResult } from 'typeorm';
import { QrDto } from '../dto/qr.dto';
import { Qr } from '../entity/qr.entity';
import { QrService } from '../service/qr.service';
export declare class QrController {
    private qrService;
    constructor(qrService: QrService);
    findAllQr(): Promise<Qr[]>;
    findByUuid(uuid: string): Promise<Qr>;
    create(dto: QrDto): Promise<Qr>;
    delete(uuid: string): Promise<DeleteResult>;
}
