import { DeleteResult, Repository } from 'typeorm';
import { QrDto } from '../dto/qr.dto';
import { Qr } from '../entity/qr.entity';
export declare class QrService {
    private qrRepository;
    constructor(qrRepository: Repository<Qr>);
    findAll(): Promise<Qr[]>;
    findOne(uuid: string): Promise<Qr>;
    create(dto: QrDto): Promise<Qr>;
    remove(uuid: string): Promise<DeleteResult>;
}
