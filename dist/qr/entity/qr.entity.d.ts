export declare class Qr {
    uuid: string;
    user: string;
    type: string;
    expires: string;
    uses: string;
    isActive: boolean;
}
