import { Repository } from "typeorm";
import { CreateUserDto } from "../dto/users.dto";
import { User } from "../entity/user.entity";
export declare class UsersService {
    private userRepository;
    constructor(userRepository: Repository<User>);
    findByEmail(email: string): Promise<User>;
    findById(id: number): Promise<User>;
    create(user: CreateUserDto): Promise<User>;
    delete(user: User): Promise<User>;
}
