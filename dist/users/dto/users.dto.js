"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateUserDto = void 0;
const openapi = require("@nestjs/swagger");
class CreateUserDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { email: { required: true, type: () => String }, name: { required: true, type: () => String }, password: { required: true, type: () => String }, avatar: { required: true, type: () => String } };
    }
}
exports.CreateUserDto = CreateUserDto;
exports.default = CreateUserDto;
//# sourceMappingURL=users.dto.js.map