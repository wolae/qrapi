export declare class User {
    id: string;
    name: string;
    avatar: string;
    email: string;
    hashPassword(): void;
    password: string;
}
